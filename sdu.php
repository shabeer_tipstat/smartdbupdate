<?php
class sdu{

    private $conn;
    private $db;
    public $new_table;
    public $new_field;
    public $alter;
    public $delete_table;
    public $delete_field;
    public $schema;	
    function __construct()
    {
        require_once("config.php");
        $this->conn=new mysqli($cfg['host'], $cfg['user'], $cfg['password']);
        $this->db=$cfg['database'];
        if(file_exists('schema.json')){
            $this->schema=json_decode(file_get_contents('schema.json'),true);
        }
    }
	
    public function create_json()
    {
        $result=$this->conn->query('SHOW TABLES');
        $rows = $result->fetch_all();
        $struct=[];
        foreach($rows as $r)
        {
            $res=$this->conn->query('DESCRIBE '.$r[0].';');	
            $struct[$r[0]]=$res->fetch_all(MYSQLI_ASSOC);
        }
        file_put_contents('schema.json',json_encode($struct,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }
	
    public function db_check()
    {
        $this->conn->query('CREATE DATABASE IF NOT EXISTS '.$this->db.';');
        $this->conn->select_db($this->db);
    }
    /*getting table updats*/
    public function update()
    {
        $schema=$this->schema;
        foreach($schema as $table=>$cols){
            $res=$this->conn->query('DESCRIBE '.$table);
            $struct=$res->fetch_all(MYSQLI_ASSOC);
            /*associating field name*/
            $struct=$this->assoc_prime($struct,array("key"=>"Field"),true);
            $cols=$this->assoc_prime($cols,array("key"=>"Field"),true);
            /*get new field*/
            $fc=array_diff(array_keys($cols),array_keys($struct));
            if(count($fc)>0){
                foreach($fc as $f){
                    $this->new_field[$table][$f]=$cols[$f];
                }
            }
            /*get field to be deleted*/
            $fd=array_diff(array_keys($struct),array_keys($cols));
            if(count($fd)>0){
                foreach($fd as $f){
                   $this->delete_field[$table][$f]=$struct[$f];
                }
            }
            /*detect changed fields */
            foreach($struct as $st=>$s){
                foreach($cols as $co=>$c){
                    if($st==$co){
                        $change=array_diff($c,$s);
                        if(count($change)>0){
                            $this->alter[$table][$co]=$c;
                        }
                    }
                }
            }
        }
    }
    /*create table. first process to create new tables*/
    function create_table(){
        $schema=  $this->schema;
        foreach($schema as $table=>$cols){
            $keys="";
            $sql="CREATE TABLE IF NOT EXISTS `$table` (";
            foreach ($cols as $c){
                if(isset($c['Key']) && $c['Key']!=""){
                    if($c['Key']=="PRI"){
                        $keys.=" PRIMARY KEY  (`".$c['Field']."`) ";   
                    }else if($c['Key']=="UNI"){
                        if($keys!=""){
                            $keys.=", ";
                        }
                        $keys.=" UNIQUE (`".$c['Field']."`) "; 
                    }else if($c['Key']=="MUL"){

                    }

                }
                $null=" NOT NULL ";
                if(isset($c['Null'])){
                    if($c['Null']=="YES"){
                        $null=" NULL ";
                    }else if($c['Null']=="NO"){
                        $null=" NOT NULL ";
                    }
                }
                $default="";
                if(isset($c['Default']) && $c['Default']!=NULL){
                    $default=" default '".$c['Default']."' ";
                }
                $extra="";
                 if(isset($c['Extra'])){
                     $extra=$c['Extra'];
                 }
                $sql.="`".$c['Field']."` ".$c['Type'].$null.$default.$extra.", ";
            }
            $sql.=$keys." );";
            $this->conn->query($sql);
        }
    }
    /**/
    function alter_table($tables,$type="CHANGE"){
        if(count($tables)<=0){
            return;
        }
        foreach ($tables as $tab=>$col){
            $sql=" ALTER TABLE `$tab`";
            $keys="";
            foreach ($col as $c){
                if(isset($c['Key']) && $c['Key']!=""){
                    if($c['Key']=="PRI"){
                        $keys.=" PRIMARY KEY  (`".$c['Field']."`) ";   
                    }else if($c['Key']=="UNI"){
                        if($keys!=""){
                            $keys.=", ";
                        }
                        $keys.=" UNIQUE (`".$c['Field']."`) "; 
                    }else if($c['Key']=="MUL"){

                    }
                }
                $null=" NOT NULL ";
                if(isset($c['Null'])){
                    if($c['Null']=="YES"){
                        $null=" NULL ";
                    }else if($c['Null']=="NO"){
                        $null=" NOT NULL ";
                    }
                }
                $default="";
                if(isset($c['Default']) && $c['Default']!=NULL){
                    $default=" default '".$c['Default']."' ";
                }
                $extra="";
                if(isset($c['Extra'])){
                    $extra=$c['Extra'];
                }
                $rename="";
                if($type=="CHANGE"){
                    $rename=$c['Field'];
                    if(isset($c['Rename']) && $c['Rename']!=""){
                        $rename=$c['Rename'];
                    }
                    $sql.=" CHANGE `".$c['Field']."` `$rename` ".$c['Type'].$null.$default.$extra." ";
                }elseif($type=="DROP"){
                    $sql.=" DROP `".$c['Field']."` ;";
                }else {
                    $sql.=" $type `".$c['Field']."` $rename ".$c['Type'].$null.$default.$extra." ";
                }
                if(end($col)!=$c){
                    $sql.=", ";
                }
            }
            $sql.=" ;";
            echo $sql."<br/>";
            $this->conn->query($sql);
        }
    }
    /*associate array with a key*/
    function assoc_prime($array,$keyval,$multi=FALSE){
        $output=array();
        if($multi){
            for($i=0;$i<count($array);$i++){
                $output[$array[$i][$keyval['key']]]=$array[$i];
            }
        }else{
            for($i=0;$i<count($array);$i++){
                $output[$array[$i][$keyval['key']]]=$array[$i][$keyval['value']];
            }
        }
        return $output;
    }
    /*process*/
    function process(){
        $this->create_table();
        $this->update();
        $this->alter_table($this->alter, "CHANGE");
        $this->alter_table($this->new_field, "ADD");
        $this->alter_table($this->delete_field, "DROP");
        $this->create_json();
        
    }
}
$sdu = new sdu;
$sdu->db_check();
if (PHP_SAPI == "cli") {
    switch(trim($argv[1])){
        case "create":
            $sdu->create_json();
        break;
        case "update":
            $sdu->process();
        break;	
    }
}else{
    echo"<pre>";
    $sdu->process();
    print_r($sdu->alter);
    print_r($sdu->new_field);
    print_r($sdu->delete_field);
}